#include "viewer.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Viewer w;
    w.show();

    if(argc>1){
        w.open(argv[1]);
    }

    return a.exec();
}
