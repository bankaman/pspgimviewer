#ifndef VIEWER_H
#define VIEWER_H

#include <QMainWindow>

namespace Ui {
class Viewer;
}

class Viewer : public QMainWindow
{
    Q_OBJECT

public:
    explicit Viewer(QWidget *parent = 0);
    ~Viewer();

    void open(QString name);

    const int IMAGE_FORMAT_RGBA5650 = 0;
    const int IMAGE_FORMAT_RGBA5551 = 1;
    const int IMAGE_FORMAT_RGBA4444 = 2;
    const int IMAGE_FORMAT_RGBA8888 = 3;
    const int IMAGE_FORMAT_INDEX4 = 4;
    const int IMAGE_FORMAT_INDEX8 = 5;
    const int IMAGE_FORMAT_INDEX16 = 6;
    const int IMAGE_FORMAT_INDEX32 = 7;

    const int PALLETE_FORMAT_NONE = -1;
    const int PALLETE_FORMAT_RGBA5650 = 0;
    const int PALLETE_FORMAT_RGBA5551 = 1;
    const int PALLETE_FORMAT_RGBA4444 = 2;
    const int PALLETE_FORMAT_RGBA8888 = 3;

    const int PIXEL_ORDER_NORMAL = 0;
    const int PIXEL_ORDER_FAST = 1;

private:

    QImage canvas,scale;

    void error(QString message);
    int read_int(QByteArray &arr, int &cur);
    int read_word(QByteArray &arr, int &cur);

    Ui::Viewer *ui;

    // QWidget interface
protected:
    void paintEvent(QPaintEvent *event);
private slots:
    void on_openAction_triggered();
    void on_exportToPNGAction_triggered();
};

#endif // VIEWER_H
