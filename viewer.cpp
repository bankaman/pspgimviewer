#include "viewer.h"
#include "ui_viewer.h"
#include <QDebug>
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
#include <QPainter>

Viewer::Viewer(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Viewer)
{
    ui->setupUi(this);

}

Viewer::~Viewer()
{
    delete ui;
}



void Viewer::open(QString name)
{
    QFile f;
    f.setFileName(name);
    f.open(QFile::ReadOnly);

    QByteArray data = f.readAll();
    f.close();

    bool l_endian=true;

    int cur=0;
    QString mig = data.mid(0,4);
    if(mig=="MIG.")
        l_endian=true;
    if(mig==".GIM")
        l_endian=false;

    if(!l_endian){
        error("This file is in big endian format. Sorry, i cannot open it.");
        return;
    }

    if(mig!=".GIM" && mig != "MIG."){
        error("This is not GIM!");
        return;
    }

    //skipping header
    cur+=4*4;

    int part_id,part_size,base;
    int image_format;
    int pixels_address;
    int pallete_address;
    int pixel_order;
    int color_depth;
    int width;
    int height;
    int real_width;
    int real_height;
    int pallete_format=PALLETE_FORMAT_NONE;
    int pallete_size;

    bool end=false;
    while(cur<data.length() && !end){
        base=cur;
        part_id = read_int(data,cur);
        cur+=4;
        part_size = read_int(data,cur);
        qDebug()<<"part_id: "<<part_id<<" size:"<<part_size<<" At 0x"+QString::number(base,16).toUpper();




        switch (part_id) {
            case 0x2: //eof addr part

            break;
            case 0x3:{ //eof addr part
                //cur=base+4;
                //int file_info = read_int(data,cur);
                //error("file_info:"+QString::number(file_info));
            }break;

            case 0x4:{ // image data
                cur=base+0x14;
                image_format = read_word(data,cur);
                pixel_order = read_word(data,cur);
                width = read_word(data,cur);
                height = read_word(data,cur);
                color_depth = read_word(data,cur);



                //error("width:"+QString::number(width)+" Height:"+QString::number(height));

                //color_depth = read_word();
                pixels_address = base+0x50;
                //error("Pixel order: 0x"+QString::number(pixel_order,16));

            }break;

            case 0x5:{ //pallete
                cur = base+0x14;
                pallete_format = read_word(data,cur);
                cur+=2;
                pallete_size = read_word(data,cur);
                pallete_address = base+0x50;

            }break;
            case 0xFF: //file info
            break;
            default:
                error("unknown part id: "+QString::number(part_id)+" At 0x"+QString::number(base,16).toUpper());
                end=true;
            break;
        }

        cur=base+part_size;
    }

    real_width=width;
    real_height=height;

    if(pixel_order==PIXEL_ORDER_FAST){
        while(real_width%16 != 0)
            real_width++;
        while(real_height%8 != 0)
            real_height++;
    }

    //drawing image
    if(pixel_order==PIXEL_ORDER_NORMAL){
        canvas = QImage(width,height,QImage::Format_RGBA8888);
    }else{
        canvas = QImage(real_width,real_height,QImage::Format_RGBA8888);
    }
    QColor c;
    QPainter p;
    p.begin(&canvas);
    p.fillRect(0,0,real_width,real_height,Qt::red);
    p.end();

    bool ok=false;
    //error("color_depth:"+QString::number(color_depth));

    if(image_format == IMAGE_FORMAT_INDEX8){
        ok=true;
        int pc, index;
        cur=pixels_address;
        int x=0;
        int y=0;
        int sx=0,sy=0;

        for(int h=0;h<real_height;h++){
            for(int w=0;w<real_width;w++){
                if(pallete_format==PALLETE_FORMAT_RGBA8888){
                    index = data.at(cur) & 0xFF;
                    pc=pallete_address+(index*4);
                    c.setRed(data.at(pc)&0xFF);
                    c.setGreen(data.at(pc+1)&0xFF);
                    c.setBlue(data.at(pc+2)&0xFF);
                    c.setAlpha(data.at(pc+3)&0xFF);
                }else{
                    error("Unknown pallete format: "+QString::number(pallete_format));
                    return;
                }

                //c.setRed(255);
                if(pixel_order==PIXEL_ORDER_NORMAL){
                    canvas.setPixelColor(w,h,c);
                }else{
                    //canvas.setPixelColor(w,h,c);
                    canvas.setPixelColor(x+sx*16,y+sy*8,c);
                    x++;
                    if(x>=16){
                        y++;
                        x=0;
                    }
                    if(y>=8){
                        y=0;
                        sx++;
                    }
                    if(sx*16>=width){
                        sx=0;
                        sy++;

                    }

                }
                cur++;
            }
        }

    }

    if(image_format==IMAGE_FORMAT_RGBA5650){
        ok=true;
        cur=pixels_address;
        int color = 0;
        int x=0;
        int y=0;
        int sx=0,sy=0;
        if(pixel_order=PIXEL_ORDER_FAST){
            real_width=width;
            while(real_width%8!=0)
                real_width++;
        }

        canvas = QImage(real_width,real_height,QImage::Format_RGBA8888);

        for(int h=0;h<real_height;h++){
            for(int w=0;w<real_width;w++){
                color = read_word(data,cur);
                c.setBlue(((color & 0xf800) >> 11)*0xff / 0x1f);
                c.setGreen(((color & 0x7E0) >> 5)*0xff / 0x3f);
                c.setRed((color & 0x1f)*0xff / 0x1f);
                c.setAlpha(255);
                if(pixel_order==PIXEL_ORDER_NORMAL){
                    canvas.setPixelColor(w,h,c);
                }else{
                    //canvas.setPixelColor(w,h,c);
                    canvas.setPixelColor(x+sx*8,y+sy*8,c);
                    x++;
                    if(x>=8){
                        y++;
                        x=0;
                    }
                    if(y>=8){
                        y=0;
                        sx++;
                    }
                    if(sx*8>=width){
                        sx=0;
                        sy++;
                        //h=height;break;

                    }

                }
            }
        }

    }

    if(image_format==IMAGE_FORMAT_RGBA8888){
        ok=true;
        cur=pixels_address;
        int x=0;
        int y=0;
        int sx=0,sy=0;

        if(pixel_order=PIXEL_ORDER_FAST){
            real_width=width;
            while(real_width%4!=0)
                real_width++;
        }
        canvas = QImage(real_width,real_height,QImage::Format_RGBA8888);

        for(int h=0;h<real_height;h++){
            for(int w=0;w<real_width;w++){
                c.setRed(data.at(cur)&0xff);cur++;
                c.setGreen(data.at(cur)&0xff);cur++;
                c.setBlue(data.at(cur)&0xff);cur++;
                c.setAlpha(data.at(cur)&0xff);cur++;
                if(pixel_order==PIXEL_ORDER_NORMAL){
                    canvas.setPixelColor(w,h,c);
                }else{
                    //canvas.setPixelColor(w,h,c);
                    canvas.setPixelColor(x+sx*4,y+sy*8,c);
                    x++;
                    if(x>=4){
                        y++;
                        x=0;
                    }
                    if(y>=8){
                        y=0;
                        sx++;
                    }
                    if(sx*4>=width){
                        sx=0;
                        sy++;

                    }

                }
            }
        }
    }

    //error("pixel order:"+QString::number(pixel_order));

    if(!ok)
        error("Sorry, i dont know how to draw this image format "+QString::number(image_format));




}

void Viewer::error(QString message)
{
    QMessageBox box;
    box.setText(message);
    box.exec();
}

int Viewer::read_int(QByteArray &arr, int &cur)
{
    int out=0;

    out= 0xff & arr.at(cur);
    out|=(0xff & arr.at(cur+1)) << 8;
    out|=(0xff & arr.at(cur+2)) << 16;
    out|=(0xff & arr.at(cur+3)) << 24;

    cur+=4;
    return out;
}

int Viewer::read_word(QByteArray &arr, int &cur)
{
    int out=0;

    if(cur+1>arr.size())
        return out;

    out =0xff & arr.at(cur);
    out|=(0xff & arr.at(cur+1)) << 8;

    cur+=2;
    return out;
}

void Viewer::paintEvent(QPaintEvent *event)
{
    QPainter p(this);
    int width = canvas.width();
    int height = canvas.height();

    if(height==0)
        return;
    width = this->width();
    height = width*canvas.height()/canvas.width();
    if(height>this->height()){
        height = this->height();
        width = height*canvas.width()/canvas.height();
    }

    scale = canvas.scaled(width,height,Qt::IgnoreAspectRatio,Qt::SmoothTransformation);
    p.drawImage(this->width()/2-scale.width()/2,this->ui->menuBar->height(),scale);
}

void Viewer::on_openAction_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,
        "Open GIM file", nullptr, "GIM Files (*.gim)");
    if(!fileName.isNull()){
        open(fileName);
    }

}

void Viewer::on_exportToPNGAction_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(this,
        "Save png file", nullptr, "PNG Files (*.png)");
    if(!fileName.isNull()){
        canvas.save(fileName,"PNG");
    }
}
